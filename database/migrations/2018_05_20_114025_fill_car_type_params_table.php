<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FillCarTypeParamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            ['name' => 'Марка машины', 'code' => 'car_brand', 'type' => 'string', 'car_type_id' => 1],
            ['name' => 'Высота стрелы', 'code' => 'boom_height', 'type' => 'integer', 'car_type_id' => 1],
            ['name' => 'Грузоподъемность стрелы', 'code' => 'lifting_capacity', 'type' => 'integer', 'car_type_id' => 1],
            ['name' => 'Марка грузоподъемного механизма', 'code' => 'lift_brand', 'type' => 'string', 'car_type_id' => 1],
            ['name' => 'Тип стрелы', 'code' => 'boom_type', 'type' => 'string', 'car_type_id' => 1],
        ];
        DB::table('pip_profile_car_type_params')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
