<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetOnlyMoskowCitiesActive extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $citiesDataList = file('database/migrations/moscow_cities');
        $cities = [];
        foreach ($citiesDataList as $row) {
            if (!empty($row)) {
                if (sizeof(explode('-', $row)) > 1)
                $cities[] = [
                    'country_id' => '185',
                    'name' => trim(explode('-', $row)[0]),
                    'code' => str_slug(trim(explode('-', $row)[0]))
                ];
            }
        }
        $cities[] = [
            'country_id' => '185',
            'name' => 'Москва',
            'code' => 'moscow'
        ];

        DB::table('rainlab_location_states')->where('country_id', '=', '185')->delete();
        DB::table('rainlab_location_states')->insert($cities);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
