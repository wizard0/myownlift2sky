<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;


class NameUpdateRURainlabLocationCountries extends Migration
{
    /**
     * Run the migrations.
     *kek
     * @return void
     */
    public function up()
    {
        Db::table('rainlab_location_countries')
            ->where('id', 185)
            ->update(['name' => 'Российская Федерация']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Db::table('rainlab_location_countries')
            ->where('id', 185)
            ->update(['name' => 'Russian Federation']);
    }
}
