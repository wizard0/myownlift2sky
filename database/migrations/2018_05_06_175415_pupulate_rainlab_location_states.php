<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class PupulateRainlabLocationStates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $cities = file('database/migrations/russian_cities');
        foreach ($cities as $key=>$value) {
            $cities[$key] = [
                'country_id' => 185,
                'name' => $value,
                'code' => str_slug($value)
            ];
        }

        DB::table('rainlab_location_states')->insert($cities);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
