<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCarTypeIdInParams extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('pip_profile_car_type_params')
            ->where('car_type_id', 1)
            ->update(
                ['car_type_id' =>
                    \PIP\Profile\Models\CarType::where('code', \PIP\Profile\Models\CarType::CAR_TYPE_CRANE)->first()->id
                ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
