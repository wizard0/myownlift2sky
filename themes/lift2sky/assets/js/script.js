
$(function() {
    "use strict";

    /*-----------------------------------
     * FIXED  MENU - HEADER
     *-----------------------------------*/
    function menuscroll() {
        var $navmenu = $('.nav-menu');
        if ($(window).scrollTop() > 50) {
            $navmenu.addClass('is-scrolling');
        } else {
            $navmenu.removeClass("is-scrolling");
        }
    }
    menuscroll();
    $(window).on('scroll', function() {
        menuscroll();
    });
    /*-----------------------------------
     * NAVBAR CLOSE ON CLICK
     *-----------------------------------*/

    $('.navbar-nav > li:not(.dropdown) > a').on('click', function() {
        $('.navbar-collapse').collapse('hide');
    });
    /*
     * NAVBAR TOGGLE BG
     *-----------------*/
    var siteNav = $('#navbar');
    siteNav.on('show.bs.collapse', function(e) {
        $(this).parents('.nav-menu').addClass('menu-is-open');
    })
    siteNav.on('hide.bs.collapse', function(e) {
        $(this).parents('.nav-menu').removeClass('menu-is-open');
    })

    /* Magnific Popup */

    $(document).ready(function() {
    	$('.popup-with-form').magnificPopup({
    		type: 'inline',
    		preloader: false,
    		focus: '#name',

    		// When elemened is focused, some mobile browsers in some cases zoom in
    		// It looks not nice, so we disable it:
    		callbacks: {
    			beforeOpen: function() {
    			    var id = $(this.st.el).data('request-data');
    			    var form = $(this.st.el).attr('href');

    			    $(form + " input[name='id']").val(id);

    			    if($(window).width() < 700) {
    					this.st.focus = false;
    				} else {
    					this.st.focus = '#name';
    				}
    			}
    		}
    	});
    });


    /*
     * Form input range
     *-----------------*/
     $('.range').on('input change', function(){
         $(this).next($('.range-label')).html(this.value); // + ' руб.'
       });
     $('.range-label').each(function(){
         var value = $(this).prev().attr('value');
         $(this).html(value);
       });

     /*
      * Form sign in toggle hidden area
      *-----------------*/
      $('.toggle-hidden').on('click', function(){
        $('.hidden-content').slideToggle();
        return false;
      });

     /*
      * Form toggle button active class
      *-----------------*/
      $('form .btn-holder .button').on('click', function(){
        $(this).toggleClass('active');
        $(this).siblings().removeClass('active');
      });

    $('form .btn-holder.for-boom_height .button').on('click', function(){
        $('input.boom_height-input').val($(this).html());
    });
    $('form .btn-holder.for-lifting_capacity .button').on('click', function(){
        $('input.lifting_capacity-input').val($(this).html());
    });
    $('input.checkbox2ban.without-popup').on('click', function () {
        $.request('onBan', {data: {id: $(this).data('request-data')}});
    });
    $('button.editCarTypeParam').on('click', function () {
        $('form#type-param-edit input[name="param_id"]').val($(this).data('id'));
        $('form#type-param-edit input[name="name"]').val($(this).data('name'));
        $('form#type-param-edit input[name="code"]').val($(this).data('code'));
        $('form#type-param-edit input[name="type"]').val($(this).data('type'));
        if (!$('.typeParamEditor').is(':visible'))
             $('button.activateTypeParamEditor').click();
        $('button.activateTypeParamEditor').html('Редактировать параметр');
        $('form#type-param-edit button[type="submit"]').html('Сохранить');
    });
    $('form#type-param-edit button.drop-form-btn').on('click', function () {
        $('#type-param-edit input[name="param_id"]').val('0');
        $('button.activateTypeParamEditor').html('Добавить параметр');
        $('form#type-param-edit button[type="submit"]').html('Добавить');
    })

    $('#partialCountryState').on('click', '.city-near-moscow', function () {
        $('.location-cities-block').show();
    });
    $('#partialCountryState').on('click', '.city-moscow', function () {
        $('.location-cities-block').hide();
    });
    $('#partialCountryState').ready(function () {
        var id = $('#partialCountryState .city-moscow').val();
        $('#partialCountryState option[value=' + id + ']').hide();
    })
}); /* End Fn */
