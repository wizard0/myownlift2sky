<?php
namespace PIP\Lift2SkyAdmin\Components;

use Cms\Classes\ComponentBase;
use Flash;
use Redirect;
use PIP\Profile\Models\CarType;
use PIP\Profile\Models\CarTypeParams;

use Input;
use Schema;

class CarTypeEdit extends ComponentBase
{

    public $params;
    public $pageName;
    public $table;
    public $type_id;
    public $type;

    /**
     * Returns information about this component, including name and description.
     */
    public function componentDetails()
    {
        return [
            'name' => 'Редактор параметров спецтехники',
            'description' => 'Форма для создания/изменения/удаления параметров для различных типов спецтехники'
        ];
    }

    public function onRun()
    {
        $this->type = CarType::find($this->param('id'));
        $this->params = $this->loadAll($this->type->id);
        $this->pageName = $this->type->name;
    }

    protected function loadAll($carTypeID)
    {
        $data = CarTypeParams::where('car_type_id', $carTypeID)->get();

        return $data;
    }

    protected function upParam($typeId, $name, $value_type, $code, $paramId = null)
    {
        if ($paramId && $paramId > 0)
            $carTypeParam = CarTypeParams::find($paramId);
        else
            $carTypeParam = new CarTypeParams();

        $carTypeParam->name = $name;
        $carTypeParam->code = $code;
        $carTypeParam->type = $value_type;
        $carTypeParam->car_type_id = $typeId;
        if ($carTypeParam->save())
            return true;
        else return false;
    }

    public function onTypeParamUp()
    {
        $paramId = Input::get('param_id');
        if ($this->upParam(
            Input::get('car_type_id'),
            Input::get('name'),
            Input::get('value_type'),
            Input::get('code'),
            $paramId
        )) {
            if ($paramId && $paramId > 0)
                Flash::success('Вы успешно изменили параметр');
            else
                Flash::success('Вы успешно добавили новый параметр');
        } else
            if ($paramId && $paramId > 0)
                Flash::error('Изменить параметр не удалось');
            else
                Flash::error('Добавить параметр не удалось');

        return Redirect::back();
    }

    public function onTypeParamDel()
    {
        if (CarTypeParams::find(Input::get('id'))->delete())
            Flash::success('Параметр успешно удален');
        else
            Flash::error('Удалить параметр не удалось');

        return Redirect::back();
    }
}
