<?php

namespace PIP\Lift2SkyAdmin\Components;

use Cms\Classes\ComponentBase;

use PIP\Lift2SkyAdmin\Models\Config;
use PIP\Profile\Models\Request;

use Input;
use Flash;
use Redirect;

class RequestsForAdmin extends ComponentBase
{
    public $allRequests;
    public $pendingRequests;
    public $acceptedRequests;
    public $completedRequests;
    public $requestPhone;
    /**
     * Returns information about this component, including name and description.
     */
    public function componentDetails()
    {
        return [
            'name' => 'Заявки для администратора',
            'description' => 'Заявки в интерфейсе администратора'
        ];
    }

    public function onRun()
    {
        $this->allRequests = $this->loadAll();
        $this->completedRequests = $this->loadAll(Request::REQUEST_STATUS_COMPLETED);
        $this->acceptedRequests = $this->loadAll(Request::REQUEST_STATUS_ACCEPTED);
        $this->pendingRequests = $this->loadAll(Request::REQUEST_STATUS_PENDING);
        $this->requestPhone = $this->getRequestPhone();
    }

    protected function getRequestPhone()
    {
        return Config::where('name', Config::REQUEST_PHONE)->first();
    }

    protected function loadAll($byStatus = false)
    {
        if (!$byStatus) {
            $query = Request::All();
        } else {
            switch ($byStatus) {
                case Request::REQUEST_STATUS_PENDING:
                    $query = Request::where('status', Request::REQUEST_STATUS_PENDING)->get();
                    break;

                case Request::REQUEST_STATUS_ACCEPTED:
                    $query = Request::where('status', Request::REQUEST_STATUS_ACCEPTED)->get();
                    break;

                case Request::REQUEST_STATUS_COMPLETED:
                    $query = Request::where('status', Request::REQUEST_STATUS_COMPLETED)->get();
                    break;
            }
        }

        return $query;
    }

    // TODO. Убрать ненужный код
    public function onAdminRequestAction()
    {
        $request = Request::find(Input::get('id'));
        $action = Input::get('action');

        switch ($action) {
            case 'edit':
                Flash::success("что-то-там");

                break;

            case 'drop':
                $request->delete();
                Flash::success('Успешное удаление зaявки');
                break;
        }

        return Redirect::back();
    }

    public function onRequestPhoneSave()
    {
        $phoneNum = Input::get(Config::REQUEST_PHONE);
        $active = Input::get('request_phone_active') == 'on';
        $config = Config::where('name', Config::REQUEST_PHONE)->first();
        if (!$config || !$config->value){
            $config = new Config();
            $config->name = Config::REQUEST_PHONE;
        }

        $config->value = $phoneNum;
        $config->active = $active;
        $config->save();

        Flash::success('Номер был успешно изменен');
        return Redirect::back();
    }
}
