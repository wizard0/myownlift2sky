<?php
namespace PIP\Lift2SkyAdmin\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Schema;
use PIP\Profile\Models\Car;
use PIP\Profile\Models\CarType;
//use CarCrane;

use Input;
use Flash;
use Redirect;

class CarTypes extends ComponentBase
{
    public $types;
    /**
     * Returns information about this component, including name and description.
     */
    public function componentDetails()
    {
        return [
            'name' => 'Типы спецтехники',
            'description' => 'Возможность добавлять, удалять и изменять виды спецтехники'
        ];
    }

    public function onRun()
    {
        $this->types = $this->loadAll();
    }

    protected function loadAll()
    {
        return CarType::All();
    }

    public function onCarTypeAdd()
    {
        $model = new CarType();
        $model->name = Input::get('name');
        $model->code = Input::get('code');
        $model->save();

        Flash::success("Новый тип успешно добавлен");
        return Redirect::back();
    }

    public function onTypeDel()
    {
        $model = CarType::find(Input::get('id'));
        $model->delete();

        Flash::success("Тип удален");
        return Redirect::back();
    }
}
