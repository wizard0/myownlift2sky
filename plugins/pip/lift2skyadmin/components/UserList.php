<?php

namespace PIP\Lift2SkyAdmin\Components;

use Cms\Classes\ComponentBase;
use PIP\Profile\Models\UserBanReason;
use RainLab\User\Models\User;

use Input;
use Flash;
use Redirect;
use Auth;

class UserList extends ComponentBase
{

    public $allUsers;

    /**
     * Returns information about this component, including name and description.
     */
    public function componentDetails()
    {
        return [
            'name' => 'Список пользователей',
            'description' => 'Вывод списка пользователей и возможность их редактировать'
        ];
    }

    public function onRun()
    {
        $this->allUsers = $this->loadAll();
    }

    protected function loadAll()
    {
        $query = User::All();

        return $query;
    }

    public function onBan()
    {
        $user = User::find(Input::get('id'));

        if (!$user->isBanned()) {
            if ($user->id == Auth::getUser()->id) {
                Flash::error('Не пытайтесь заблокировать себя');
                return Redirect::back();
            }

            $user->ban();
            $ban = new UserBanReason();
            $ban->reason = Input::get('comment');
            $ban->user_id = Input::get('id');
            $ban->save();

            Flash::success('Пользователь заблокирован');
        } else {
            $user->unBan();
            $ban = UserBanReason::where('user_id', Input::get('id'));
            $ban->delete();

            Flash::success('Пользователь разблокирован');
        }

        return Redirect::back();
    }
}
