<?php
namespace PIP\Lift2SkyAdmin\Components;

use Cms\Classes\ComponentBase;
use RainLab\User\Models\User;
use Redirect;
use Flash;
use Input;

class UserEditForm extends ComponentBase
{
    public $userData;
    /**
     * Returns information about this component, including name and description.
     */
    public function componentDetails()
    {
        return [
            'name' => 'Форма редактирования пользователя',
            'description' => 'Форма для админа, в которой он редактирует пользователей'
        ];
    }

    public function onRun()
    {
        $this->userData = $this->getUser($this->param('id'));
    }

    protected function getUser($id)
    {
        $query = User::find($id);

        return $query;
    }

    public function onSave()
    {
        $user = User::find(Input::get('id'));
        $user->name = Input::get('name');
        $user->surname = Input::get('surname');
        $user->username = Input::get('username');
        $user->email = Input::get('email');
        $user->phone = Input::get('phone');
        $user->save();

        Flash::success('Запись упешно обновлена');
        return Redirect::back();
    }
}
