<?php namespace PIP\Lift2SkyAdmin\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePipLift2skyadminConfig extends Migration
{
    public function up()
    {
        Schema::table('pip_lift2skyadmin_config', function($table)
        {
            $table->smallInteger('active');
            $table->increments('id')->unsigned(false)->change();
            $table->string('name')->change();
            $table->string('value')->change();
        });
    }
    
    public function down()
    {
        Schema::table('pip_lift2skyadmin_config', function($table)
        {
            $table->dropColumn('active');
            $table->increments('id')->unsigned()->change();
            $table->string('name', 191)->change();
            $table->string('value', 191)->change();
        });
    }
}
