<?php namespace PIP\Lift2SkyAdmin\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePipLift2skyadminConfig extends Migration
{
    public function up()
    {
        Schema::create('pip_lift2skyadmin_config', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('value');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pip_lift2skyadmin_config');
    }
}
