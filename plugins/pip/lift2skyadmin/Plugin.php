<?php namespace PIP\Lift2SkyAdmin;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'PIP\Lift2SkyAdmin\Components\RequestsForAdmin' => 'adminRequests',
            'PIP\Lift2SkyAdmin\Components\UserList' => 'userlist',
            'PIP\Lift2SkyAdmin\Components\UserEditForm' => 'userEditForm',
            'PIP\Lift2SkyAdmin\Components\CarTypes' => 'carTypes',
            'PIP\Lift2SkyAdmin\Components\CarTypeEdit' => 'carTypeEdit'
        ];
    }

    public function registerSettings()
    {
    }
}
