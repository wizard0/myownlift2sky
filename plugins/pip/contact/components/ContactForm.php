<?php
namespace PIP\Contact\Components;

use Cms\Classes\ComponentBase;
use PIP\Contact\Models\Callme;

use Auth;
use Input;
use Flash;
use Redirect;
use Validator;

class ContactForm extends ComponentBase
{

    /**
     * Returns information about this component, including name and description.
     */
    public function componentDetails()
    {
        return [
            'name' => 'Форма обратной связи',
            'description' => 'Простая форма обратной связи',
        ];
    }

    public function onCallme()
    {
        $validator = Validator::make(
            [
                'phone' => Input::get('tel')
            ],
            [
                'phone' => 'required'
            ]
        );
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $callme = new Callme();
        $callme->phone = Input::get('tel');
        if (Auth::check())
            $callme->user_id = Auth::getUser()->id;

        $callme->save();

        Flash::success('Мы вам обязательно перезвоним');
        return Redirect::back();
    }
}
