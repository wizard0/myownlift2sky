<?php
namespace PIP\Contact\Components;

use Cms\Classes\ComponentBase;
use PIP\Contact\Models\Callme;

class ContactList extends ComponentBase
{
    public $contacts;
    /**
     * Returns information about this component, including name and description.
     */
    public function componentDetails()
    {
        return [
            'name' => 'Список контактов',
            'description' => 'Данные оставленные пользователями в форме "Перезвони мне"'
        ];
    }

    public function onRun()
    {
        $this->contacts = $this->loadAll();
    }

    protected function loadAll()
    {
        $query = Callme::All();

        return $query;
    }
}
