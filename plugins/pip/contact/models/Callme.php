<?php namespace PIP\Contact\Models;

use Model;

/**
 * Model
 */
class Callme extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'user' => 'RainLab\User\Models\User'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pip_contact_callme';
}
