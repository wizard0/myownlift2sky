<?php namespace PIP\Contact\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePipContactCallme extends Migration
{
    public function up()
    {
        Schema::table('pip_contact_callme', function($table)
        {
            $table->integer('user_id')->nullable();
            $table->increments('id')->unsigned(false)->change();
            $table->string('phone')->change();
        });
    }
    
    public function down()
    {
        Schema::table('pip_contact_callme', function($table)
        {
            $table->dropColumn('user_id');
            $table->increments('id')->unsigned()->change();
            $table->string('phone', 191)->change();
        });
    }
}
