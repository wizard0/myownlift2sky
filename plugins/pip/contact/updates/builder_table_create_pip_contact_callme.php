<?php namespace PIP\Contact\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePipContactCallme extends Migration
{
    public function up()
    {
        Schema::create('pip_contact_callme', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('phone');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pip_contact_callme');
    }
}
