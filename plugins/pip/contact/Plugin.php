<?php namespace PIP\Contact;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'PIP\Contact\Components\ContactForm' => 'contactform',
            'PIP\Contact\Components\ContactList' => 'contactList',
        ];
    }

    public function registerSettings()
    {
    }
}
