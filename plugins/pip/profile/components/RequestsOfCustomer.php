<?php
/**
 * Created by PhpStorm.
 * User: wizard
 * Date: 4/24/18
 * Time: 8:22 PM
 */

namespace PIP\Profile\Components;

use Cms\Classes\ComponentBase;
use PIP\Profile\Models\Request;

use Auth;


class RequestsOfCustomer extends ComponentBase
{

    public $requests;

    /**
     * Returns information about this component, including name and description.
     */
    public function componentDetails()
    {
        return [
            'name' => 'Заявки заказчика',
            'description' => 'Список заявок для интерфейса заказчика'
        ];
    }

    public function defineProperties()
    {
        return [
            'results' => [
                'title' => 'Number of requests',
                'description' => 'How many requests do you want to display',
                'default' => 0, // all of requests,
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'Only numbers allowed'
            ],
            'sortOrder' => [
                'title' => 'Sort requests',
                'description' => 'Sort those requests',
                'type' => 'dropdown',
                'default' => 'customer_id asc',

            ]
        ];
    }

    public function getSortOrderOptions() {
        return [
            'customer_id asc' => 'Customer_id (ascending)',
            'customer_id desc' => 'Customer_id (descending)'
        ];
    }

    public function onRun() {
        $this->requests = $this->loadRequests();
    }

    protected function loadRequests() {
        $query = Request::where('customer_id', Auth::getUser()->id)->get();

        if ($this->property('sortOrder') == 'customer_id asc') {
            $query = $query->sortBy('customer_id');
        }

        if ($this->property('sortOrder') == 'customer_id desc') {
            $query = $query->sortByDesc('customer_id');
        }

        if ($this->property('results') > 0) {
            $query = $query->take($this->property('results'));
        }

        return $query;
    }
}