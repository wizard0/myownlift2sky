<?php
/**
 * Created by PhpStorm.
 * User: wizard
 * Date: 4/28/18
 * Time: 2:32 PM
 */

namespace PIP\Profile\Components;

use Cms\Classes\ComponentBase;
use PIP\Profile\Models\Car;
use PIP\Profile\Models\CarCrane;

use Input;
use Auth;
use PIP\Profile\Models\CarTypeParams;
use PIP\Profile\Models\OwnerCarParams;
use PIP\Profile\Models\CarType;
use Session;

use Validator;
use Redirect;
use Flash;


class OwnerCars extends ComponentBase
{

    public $cars;

    /**
     * Returns information about this component, including name and description.
     */
    public function componentDetails()
    {
        return [
            'name' => 'Мои автовышки',
            'description' => 'Форма просмотра, создания и редактирования автовышек'
        ];
    }

    public function onRun() {
        $this->cars =  $this->formatParamsArray($this->loadCars());
    }

    private function formatParamsArray($cars) {
        $paramsArray = [];

        foreach ($cars as $car) {
            foreach ($car->params as $param) {
                $paramsArray[$car->id][$param->param->code] = $param->value;
            }
            $paramsArray[$car->id]['id'] = $car->id;
            $paramsArray[$car->id]['status'] = ($car->status == Car::CAR_STATUS_FREE);
        }
        return $paramsArray;
    }

    private function loadCars() {
        $query = Car::where('owner_id', Auth::getUser()->id)->get();

        return $query;
    }

    public function onCarSave() {
        $requestData = Input::all();

        $validator = Validator::make(
            [
                'car_brand' => $requestData['car_brand'],
                'boom_height' => $requestData['boom_height'],
                'lifting_capacity' => $requestData['lifting_capacity'],
                'lift_brand' => $requestData['lift_brand'],
                'boom_type' => $requestData['boom_type']
            ], [
                'car_brand' => 'required',
                'boom_height' => 'required',
                'lifting_capacity' => 'required',
                'lift_brand' => 'required',
                'boom_type' => 'required'
            ]
        );
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $carType = CarType::where('code', $requestData['carType'])->first();

        $car = new Car();
        $car->owner_id = Auth::getUser()->id;
        $car->status = Car::CAR_STATUS_FREE;
        $car->car_type_id = $carType->id; // TODO. Это не правильно. Неободимо избавиться от такой связи
        $car->save();

        $typeParams = $carType->paramNames;
        foreach ($typeParams as $typeParam) {
            if (array_key_exists($typeParam->code, $requestData)) {
                $ownerCarParams = new OwnerCarParams();
                $ownerCarParams->param_id = $typeParam->id;
                $ownerCarParams->value = $requestData[$typeParam->code];
                $car->params()->save($ownerCarParams);
            }
        }


        Flash::success('Новая автовышка успешно создана');
        return Redirect::back();
    }

    public function onDelete() {
        Car::find(Input::get('id'))->delete();

        Flash::success('Автовышка удалена');
        return Redirect::back();
    }
}
