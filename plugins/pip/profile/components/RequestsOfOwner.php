<?php
/**
 * Created by PhpStorm.
 * User: wizard
 * Date: 4/24/18
 * Time: 8:22 PM
 */

namespace PIP\Profile\Components;

use Cms\Classes\ComponentBase;
use October\Rain\Support\Facades\Flash;
use PIP\Lift2SkyAdmin\Models\Config;
use PIP\Profile\Models\CanceledRequests;
use PIP\Profile\Models\CompletedRequests;
use PIP\Profile\Models\DeclinedRequests;
use PIP\Profile\Models\Request;

use Auth;
use Input;
use Redirect;


class RequestsOfOwner extends ComponentBase
{

    public $allRequests;
    public $pendingRequests;
    public $acceptedRequests;
    public $declinedRequests;
    public $completedRequests;
    public $requestPhoneConfig;

    /**
     * Returns information about this component, including name and description.
     */
    public function componentDetails()
    {
        return [
            'name' => 'Заявки исполнителя',
            'description' => 'Список заявок в интерфейсе исполнителя'
        ];
    }

    public function defineProperties()
    {
        return [
            'results' => [
                'title' => 'Number of requests',
                'description' => 'How many requests do you want to display',
                'default' => 0, // all of requests,
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'Only numbers allowed'
            ],
            'sortOrder' => [
                'title' => 'Sort requests',
                'description' => 'Sort those requests',
                'type' => 'dropdown',
                'default' => 'customer_id asc',

            ]
        ];
    }

    public function getSortOrderOptions() {
        return [
            'customer_id asc' => 'Customer_id (ascending)',
            'customer_id desc' => 'Customer_id (descending)'
        ];
    }

    public function onRun() {
        $this->allRequests = $this->loadRequests();
        $this->pendingRequests = $this->loadRequests(Request::REQUEST_STATUS_PENDING);
        $this->acceptedRequests = $this->loadRequests(Request::REQUEST_STATUS_ACCEPTED);
        $this->declinedRequests = $this->loadRequests(Request::REQUEST_STATUS_DECLINED);
        $this->completedRequests = $this->loadRequests(Request::REQUEST_STATUS_COMPLETED);

        $this->requestPhoneConfig = Config::where('name', Config::REQUEST_PHONE)->first();
    }

    protected function loadRequests($byStatus = false) {
        if ($byStatus) {
            switch ($byStatus) {
                case Request::REQUEST_STATUS_DECLINED:
                case Request::REQUEST_STATUS_COMPLETED:
                    $query = Request::whereHas($byStatus, function($query) {
                        $query->where('owner_id', Auth::getUser()->id);
                    })->get();

                    break;
                case Request::REQUEST_STATUS_ACCEPTED:
                    $query = Request::where('status', $byStatus)
                        ->where('owner_id', Auth::getUser()->id)
                        ->get();

                    break;
                case Request::REQUEST_STATUS_PENDING:
                    $not = [];
                    if ($query = Request::whereHas('declined', function($query) {
                        $query->where('owner_id', Auth::getUser()->id);
                    })->get()) {
                        foreach ($query as $q) {
                            $not[]= $q->id;
                        }
                    }
                    $query = Request::where('status', $byStatus)->whereNotIn('id', $not)->get();

                    break;
            }
        } else {
            $query = Request::All();
        }

        if ($this->property('sortOrder') == 'customer_id asc') {
            $query = $query->sortBy('customer_id');
        }

        if ($this->property('sortOrder') == 'customer_id desc') {
            $query = $query->sortByDesc('customer_id');
        }

        if ($this->property('results') > 0) {
            $query = $query->take($this->property('results'));
        }

        return $query;
    }

    public function onAction() {
        $request = Request::find(Input::get('id'));
        $action = Input::get('action');
        switch ($action) {
            case 'accept':
                if ($request->has('declined')) {
                    $request->declined()->where('owner_id', Auth::getUser()->id)
                        ->delete();
                }
                $request->status = Request::REQUEST_STATUS_ACCEPTED;
                $request->owner_id = Auth::getUser()->id;

                Flash::success('Заявка принята');
                break;
            case 'cancel':
                $request->status = Request::REQUEST_STATUS_PENDING;
                $canceledRequest = new CanceledRequests();
                $canceledRequest->comment = Input::get('comment');
                $canceledRequest->owner_id = Auth::getUser()->id;
                $request->canceled()->save($canceledRequest);

                Flash::success('Заявка отменена');
                break;
            case 'decline':
                $declinedRequest = new DeclinedRequests();
                $declinedRequest->owner_id = Auth::getUser()->id;
                $request->status = Request::REQUEST_STATUS_PENDING; // этот статус нужен для других исполнителей
                $request->declined()->save($declinedRequest);

                Flash::success('Заявка скрыта');
                break;
            case 'complete':
                $completedRequest = new CompletedRequests();
                $completedRequest->owner_id = Auth::getUser()->id;
                $request->status = Request::REQUEST_STATUS_COMPLETED;
                $request->owner_id = Auth::getUser()->id;
                $request->completed()->save($completedRequest);

                Flash::success('Заявка обработана');
                break;
        }
        $request->save();
        return Redirect::back();

    }
}
