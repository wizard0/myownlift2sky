<?php
/**
 * Created by PhpStorm.
 * User: wizard
 * Date: 4/24/18
 * Time: 9:40 PM
 */

namespace PIP\Profile\Components;

use Cms\Classes\ComponentBase;
use Cms\Classes\Page;

use Input;
use Auth;
use October\Rain\Flash\FlashServiceProvider;
use PIP\Profile\Models\CarType;
use PIP\Profile\Models\CarTypeParams;
use PIP\Profile\Models\RequestCarParams;
use Session;

use Validator;
use Redirect;
use Flash;

use PIP\Profile\Models\Request;
use PIP\Profile\Models\RequestCrane;
use RainLab\Location\Models\State;

class RequestForm extends ComponentBase
{
    public $requestCarParams;
    public $requestCarParamsData;
    public $request;
    public $requestDate;
    public $moscowCityID;
    public $default;
    /**
     * Returns information about this component, including name and description.
     */
    public function componentDetails()
    {
        return [
            'name' => 'Форма заявок',
            'description' => 'Форма заявок для главной страницы'
        ];
    }

    public function onRun()
    {
        $id = CarType::where('code', CarType::CAR_TYPE_CRANE)->first()->id;
        $this->requestCarParams = CarTypeParams::getTypeParams($id);

        $this->moscowCityID = State::where('code', 'moscow')->first()->id;
        if (
            ($this->param('id') &&
            (Auth::check() && Auth::getUser()->isAdmin()) &&
            ($this->request = Request::find($this->param('id'))))
        ) { // если админ редактирует заявку
            $this->requestDate = date_format(
                date_create_from_format("Y.m.d", $this->request->date_on),
                "Y-m-d"
            );
            foreach ($this->requestCarParams as $p) {
                $pDataSet = $this->request->params()->where('param_id', $p->id)->get();
                foreach ($pDataSet as $pData) {
                    $this->requestCarParamsData[$p->code] = $pData->value;
                }
            }

        } elseif($this->param('id') && ! (Auth::check() && Auth::getUser()->isAdmin())) {
            return Redirect::back();
        }
        $this->default['boom_height'] = [16,18, 20, 22, 24, 26, 28, 30, 32, 36, 40, 45];
        $this->default['lifting_capacity'] = [200, 240, 280, 300, 400];
        $this->default['boom_type'] = ['Телескопические', 'Коленчатые', 'Комбинированные'];
        $this->default['car_brand'] = ['Isuzu', 'Mitsubishi', 'HYUNDAI', 'Daewoo', 'Hyundai', 'Kia', 'Horyong'];
    }

    public function onSave() {
        $requestData = Input::all();

        if (!empty($requestData)) { // может быть пустым, если мы попали сюда после логина
            $validator = Validator::make(
                [
                    'date' => $requestData['date'],
                    'time' => $requestData['time'],
                    'country_id' => $requestData['country_id'],
                    'state_id' => $requestData['state_id'],
                    'address' => $requestData['address'],
                ], [
                    'date' => 'required',
                    'time' => 'required',
                    'country_id' => 'required',
                    'state_id' => 'required',
                    'address' => 'required',
                ]
            );
            if ($validator->fails()) {
                Flash::error("Ошибки!!!");
                return Redirect::back()->withErrors($validator);
            }
        }

        if (!Auth::check()) {
            Session::put('requestData', $requestData);
            Flash::warning("Необходимо войти, чтобы создать заявку");
            return Redirect::to(Page::url('login'));
        }
        else {

            if (Session::has('requestData')) {
                $requestData = Session::get('requestData');
                Session::forget('requestData');
            }

            if (Input::get('request_id')) {
                $request = Request::find(Input::get('request_id'));
            } else {
                $request = new Request();
                $request->save();
                $params = $requestData['param'];
                foreach ($params as $key => $value) {
                    $p = new RequestCarParams();
                    $p->param_id = $key;
                    $p->value = $value;
                    $request->params()->save($p);
                }
            }

            foreach ($request->params() as $param)
            {
                $param->value = $requestData['param'][$param->param_id];
                $param->save();
            }

            $request->date_on = $requestData['date'];
            $request->time_on = $requestData['time'];
            if (! Auth::getUser()->isAdmin()) {
                $request->customer_id = Auth::getUser()->id;
            }
            $request->pay_sum = $requestData['range'];
            $request->country_id = $requestData['country_id'];
            if ($requestData['state']) // это для москвы
                $request->state_id = $requestData['state'];
            else
                $request->state_id = $requestData['state_id'];
            $request->address = $requestData['address'];
            $request->save();

            if (Input::get('request_id')) {
                Flash::success('Заявка успешно отредактирована');
            } else {
                Flash::success('Ваша заявка была успешно создана');
            }

            return Redirect::back();
        }
    }
}
