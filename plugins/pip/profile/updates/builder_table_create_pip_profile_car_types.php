<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePipProfileCarTypes extends Migration
{
    public function up()
    {
        Schema::create('pip_profile_car_types', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('code');
            $table->text('description')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pip_profile_car_types');
    }
}
