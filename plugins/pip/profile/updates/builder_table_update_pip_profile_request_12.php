<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePipProfileRequest12 extends Migration
{
    public function up()
    {
        Schema::table('pip_profile_request', function($table)
        {
            $table->integer('country_id')->nullable()->unsigned();
            $table->integer('state_id')->nullable()->unsigned();
            $table->dropColumn('address_id');
        });
    }
    
    public function down()
    {
        Schema::table('pip_profile_request', function($table)
        {
            $table->dropColumn('country_id');
            $table->dropColumn('state_id');
            $table->integer('address_id')->nullable();
        });
    }
}
