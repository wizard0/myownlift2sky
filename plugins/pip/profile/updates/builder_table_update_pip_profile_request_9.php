<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePipProfileRequest9 extends Migration
{
    public function up()
    {
        Schema::table('pip_profile_request', function($table)
        {
            $table->renameColumn('request_date_on', 'date_on');
            $table->renameColumn('request_time_on', 'time_on');
        });
    }
    
    public function down()
    {
        Schema::table('pip_profile_request', function($table)
        {
            $table->renameColumn('date_on', 'request_date_on');
            $table->renameColumn('time_on', 'request_time_on');
        });
    }
}
