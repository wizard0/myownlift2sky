<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePipProfileCrane extends Migration
{
    public function up()
    {
        Schema::table('pip_profile_crane', function($table)
        {
            $table->integer('user_id');
        });
    }
    
    public function down()
    {
        Schema::table('pip_profile_crane', function($table)
        {
            $table->dropColumn('user_id');
        });
    }
}
