<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePipProfileRequestCarParams extends Migration
{
    public function up()
    {
        Schema::create('pip_profile_request_car_params', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('param_id');
            $table->string('value');
            $table->integer('request_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pip_profile_request_car_params');
    }
}
