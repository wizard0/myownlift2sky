<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePipProfileCanceledRequest extends Migration
{
    public function up()
    {
        Schema::create('pip_profile_canceled_request', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('request_id');
            $table->integer('owner_id');
            $table->text('comment')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pip_profile_canceled_request');
    }
}
