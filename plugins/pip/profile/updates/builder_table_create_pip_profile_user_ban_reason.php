<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePipProfileUserBanReason extends Migration
{
    public function up()
    {
        Schema::create('pip_profile_user_ban_reason', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->text('ban_reason');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pip_profile_user_ban_reason');
    }
}
