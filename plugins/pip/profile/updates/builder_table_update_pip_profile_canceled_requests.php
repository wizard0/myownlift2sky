<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePipProfileCanceledRequests extends Migration
{
    public function up()
    {
        Schema::rename('pip_profile_canceled_request', 'pip_profile_canceled_requests');
        Schema::table('pip_profile_canceled_requests', function($table)
        {
            $table->increments('id')->unsigned(false)->change();
        });
    }
    
    public function down()
    {
        Schema::rename('pip_profile_canceled_requests', 'pip_profile_canceled_request');
        Schema::table('pip_profile_canceled_request', function($table)
        {
            $table->increments('id')->unsigned()->change();
        });
    }
}
