<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePipProfileAcceptedRequests extends Migration
{
    public function up()
    {
        Schema::create('pip_profile_accepted_requests', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('owner_id');
            $table->integer('request_id');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pip_profile_accepted_requests');
    }
}
