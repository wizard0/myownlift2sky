<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePipProfileRequest10 extends Migration
{
    public function up()
    {
        Schema::table('pip_profile_request', function($table)
        {
            $table->renameColumn('date_on', 'date');
            $table->renameColumn('time_on', 'time');
        });
    }
    
    public function down()
    {
        Schema::table('pip_profile_request', function($table)
        {
            $table->renameColumn('date', 'date_on');
            $table->renameColumn('time', 'time_on');
        });
    }
}
