<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePipProfileRequest6 extends Migration
{
    public function up()
    {
        Schema::table('pip_profile_request', function($table)
        {
            $table->integer('status')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('pip_profile_request', function($table)
        {
            $table->dropColumn('status');
        });
    }
}
