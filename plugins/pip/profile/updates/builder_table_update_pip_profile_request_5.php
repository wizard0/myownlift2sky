<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePipProfileRequest5 extends Migration
{
    public function up()
    {
        Schema::table('pip_profile_request', function($table)
        {
            $table->date('request_date_on');
            $table->time('request_time_on');
            $table->dropColumn('request_datetime_on');
        });
    }
    
    public function down()
    {
        Schema::table('pip_profile_request', function($table)
        {
            $table->dropColumn('request_date_on');
            $table->dropColumn('request_time_on');
            $table->dateTime('request_datetime_on');
        });
    }
}
