<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeletePipProfileRequestStatus extends Migration
{
    public function up()
    {
        Schema::dropIfExists('pip_profile_request_status');
    }
    
    public function down()
    {
        Schema::create('pip_profile_request_status', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('value', 191);
        });
    }
}
