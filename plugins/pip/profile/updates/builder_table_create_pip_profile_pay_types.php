<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePipProfilePayTypes extends Migration
{
    public function up()
    {
        Schema::create('pip_profile_pay_types', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('value');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pip_profile_pay_types');
    }
}
