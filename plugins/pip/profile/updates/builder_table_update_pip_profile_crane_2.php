<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePipProfileCrane2 extends Migration
{
    public function up()
    {
        Schema::table('pip_profile_crane', function($table)
        {
            $table->smallInteger('status')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('pip_profile_crane', function($table)
        {
            $table->dropColumn('status');
        });
    }
}
