<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePipProfileCars extends Migration
{
    public function up()
    {
        Schema::create('pip_profile_cars', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('owner_id');
            $table->string('type');
            $table->integer('status')->default(1);
            $table->integer('params_id');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pip_profile_cars');
    }
}
