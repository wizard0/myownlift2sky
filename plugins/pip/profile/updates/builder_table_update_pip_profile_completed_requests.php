<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePipProfileCompletedRequests extends Migration
{
    public function up()
    {
        Schema::rename('pip_profile_complited_requests', 'pip_profile_completed_requests');
    }
    
    public function down()
    {
        Schema::rename('pip_profile_completed_requests', 'pip_profile_complited_requests');
    }
}
