<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePipProfileCarCraneParams extends Migration
{
    public function up()
    {
        Schema::table('pip_profile_car__crane_params', function($table)
        {
            $table->string('lift_brand');
            $table->increments('id')->unsigned(false)->change();
            $table->string('car_brand')->change();
        });
    }
    
    public function down()
    {
        Schema::table('pip_profile_car__crane_params', function($table)
        {
            $table->dropColumn('lift_brand');
            $table->increments('id')->unsigned()->change();
            $table->string('car_brand', 191)->change();
        });
    }
}
