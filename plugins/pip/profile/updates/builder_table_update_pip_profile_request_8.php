<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePipProfileRequest8 extends Migration
{
    public function up()
    {
        Schema::table('pip_profile_request', function($table)
        {
            $table->string('status', 10)->nullable(false)->unsigned(false)->default('pending')->change();
        });
    }
    
    public function down()
    {
        Schema::table('pip_profile_request', function($table)
        {
            $table->integer('status')->nullable(false)->unsigned(false)->default(1)->change();
        });
    }
}
