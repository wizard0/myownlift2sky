<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePipProfileRequest4 extends Migration
{
    public function up()
    {
        Schema::table('pip_profile_request', function($table)
        {
            $table->integer('pay_sum');
        });
    }
    
    public function down()
    {
        Schema::table('pip_profile_request', function($table)
        {
            $table->dropColumn('pay_sum');
        });
    }
}
