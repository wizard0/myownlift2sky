<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePipProfileCar2 extends Migration
{
    public function up()
    {
        Schema::table('pip_profile_car', function($table)
        {
            $table->dropColumn('params_id');
        });
    }
    
    public function down()
    {
        Schema::table('pip_profile_car', function($table)
        {
            $table->integer('params_id');
        });
    }
}
