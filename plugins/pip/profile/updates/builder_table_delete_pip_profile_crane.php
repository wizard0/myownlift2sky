<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeletePipProfileCrane extends Migration
{
    public function up()
    {
        Schema::dropIfExists('pip_profile_crane');
    }
    
    public function down()
    {
        Schema::create('pip_profile_crane', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('machine_brand', 254);
            $table->string('lifting_brand', 254)->nullable();
            $table->string('boom_type', 254)->nullable();
            $table->integer('lifting_capacity');
            $table->integer('boom_height');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('owner_id');
            $table->smallInteger('status')->default(0);
        });
    }
}
