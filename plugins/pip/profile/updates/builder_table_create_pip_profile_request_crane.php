<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePipProfileRequestCrane extends Migration
{
    public function up()
    {
        Schema::create('pip_profile_request_crane', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('machine_brand');
            $table->integer('boom_height');
            $table->integer('lifting_capacity');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pip_profile_request_crane');
    }
}
