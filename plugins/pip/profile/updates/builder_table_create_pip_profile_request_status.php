<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePipProfileRequestStatus extends Migration
{
    public function up()
    {
        Schema::create('pip_profile_request_status', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('value');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pip_profile_request_status');
    }
}
