<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePipProfileCrane4 extends Migration
{
    public function up()
    {
        Schema::table('pip_profile_crane', function($table)
        {
            $table->string('lifting_brand', 254)->nullable()->change();
            $table->string('boom_type', 254)->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('pip_profile_crane', function($table)
        {
            $table->string('lifting_brand', 254)->nullable(false)->change();
            $table->string('boom_type', 254)->nullable(false)->change();
        });
    }
}
