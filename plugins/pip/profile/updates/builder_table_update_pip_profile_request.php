<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePipProfileRequest extends Migration
{
    public function up()
    {
        Schema::table('pip_profile_request', function($table)
        {
            $table->integer('lease_duration_pre')->nullable()->change();
            $table->integer('contact_persons')->nullable()->change();
            $table->smallInteger('pay_type_id')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('pip_profile_request', function($table)
        {
            $table->integer('lease_duration_pre')->nullable(false)->change();
            $table->integer('contact_persons')->nullable(false)->change();
            $table->smallInteger('pay_type_id')->nullable(false)->change();
        });
    }
}
