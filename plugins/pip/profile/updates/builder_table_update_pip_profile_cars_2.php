<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePipProfileCars2 extends Migration
{
    public function up()
    {
        Schema::table('pip_profile_cars', function($table)
        {
            $table->string('status', 10)->default('free')->change();
        });
    }
    
    public function down()
    {
        Schema::table('pip_profile_cars', function($table)
        {
            $table->string('status', 10)->default(null)->change();
        });
    }
}
