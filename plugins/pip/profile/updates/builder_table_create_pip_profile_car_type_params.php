<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePipProfileCarTypeParams extends Migration
{
    public function up()
    {
        Schema::create('pip_profile_car_type_params', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('code');
            $table->string('type');
            $table->integer('car_type_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pip_profile_car_type_params');
    }
}
