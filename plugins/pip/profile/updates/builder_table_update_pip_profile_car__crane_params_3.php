<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePipProfileCarCraneParams3 extends Migration
{
    public function up()
    {
        Schema::table('pip_profile_car__crane_params', function($table)
        {
            $table->integer('car_id');
        });
    }
    
    public function down()
    {
        Schema::table('pip_profile_car__crane_params', function($table)
        {
            $table->dropColumn('car_id');
        });
    }
}
