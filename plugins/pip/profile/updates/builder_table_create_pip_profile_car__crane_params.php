<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePipProfileCarCraneParams extends Migration
{
    public function up()
    {
        Schema::create('pip_profile_car__crane_params', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('car_brand');
            $table->integer('boom_height');
            $table->integer('lifting_capacity');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pip_profile_car__crane_params');
    }
}
