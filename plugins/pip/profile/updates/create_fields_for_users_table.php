<?php
namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddNewFields extends Migration
{

	public function up()
	{
		Schema::table('users', function($table) {
			$table->string('phone')->nullable();
			$table->integer('user_type')->unsigned()->nullable();
		});
	}

	public function down()
	{
		Schema::table('users', function($table) {
			$table->dropDown([
				'phone',
				'user_type'
			]);
		});
	}
}
