<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePipProfileCarCraneParams2 extends Migration
{
    public function up()
    {
        Schema::table('pip_profile_car__crane_params', function($table)
        {
            $table->string('boom_type');
            $table->string('car_brand')->change();
            $table->string('lift_brand')->change();
        });
    }
    
    public function down()
    {
        Schema::table('pip_profile_car__crane_params', function($table)
        {
            $table->dropColumn('boom_type');
            $table->string('car_brand', 191)->change();
            $table->string('lift_brand', 191)->change();
        });
    }
}
