<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePipProfileCrane5 extends Migration
{
    public function up()
    {
        Schema::table('pip_profile_crane', function($table)
        {
            $table->renameColumn('users_id', 'owner_id');
        });
    }
    
    public function down()
    {
        Schema::table('pip_profile_crane', function($table)
        {
            $table->renameColumn('owner_id', 'users_id');
        });
    }
}
