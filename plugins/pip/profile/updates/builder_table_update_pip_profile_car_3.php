<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePipProfileCar3 extends Migration
{
    public function up()
    {
        Schema::table('pip_profile_car', function($table)
        {
            $table->dropColumn('type');
        });
    }
    
    public function down()
    {
        Schema::table('pip_profile_car', function($table)
        {
            $table->string('type', 191);
        });
    }
}
