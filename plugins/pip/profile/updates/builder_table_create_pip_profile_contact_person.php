<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePipProfileContactPerson extends Migration
{
    public function up()
    {
        Schema::create('pip_profile_contact_person', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('full_name');
            $table->text('contact_data_json');
            $table->integer('legal_entity_id');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pip_profile_contact_person');
    }
}
