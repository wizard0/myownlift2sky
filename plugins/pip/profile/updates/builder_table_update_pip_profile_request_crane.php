<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePipProfileRequestCrane extends Migration
{
    public function up()
    {
        Schema::table('pip_profile_request_crane', function($table)
        {
            $table->renameColumn('machine_brand', 'car_brand');
        });
    }
    
    public function down()
    {
        Schema::table('pip_profile_request_crane', function($table)
        {
            $table->renameColumn('car_brand', 'machine_brand');
        });
    }
}
