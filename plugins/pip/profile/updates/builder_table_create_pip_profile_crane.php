<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePipProfileCrane extends Migration
{
    public function up()
    {
        Schema::create('pip_profile_crane', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('machine_brand', 254);
            $table->string('lifting_brand', 254);
            $table->string('boom_type', 254);
            $table->integer('lifting_capacity');
            $table->integer('boom_height');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pip_profile_crane');
    }
}
