<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePipProfileRequest extends Migration
{
    public function up()
    {
        Schema::create('pip_profile_request', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('address_id');
            $table->dateTime('request_datetime_on');
            $table->integer('crane_id');
            $table->integer('crane_count');
            $table->integer('lease_duration_pre');
            $table->integer('lease_duration_fact')->nullable();
            $table->dateTime('lease_end_datetime')->nullable();
            $table->integer('customer_id');
            $table->integer('contact_persons');
            $table->smallInteger('pay_type_id');
            $table->text('commentary')->nullable();
            $table->integer('owner_id')->nullable();
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pip_profile_request');
    }
}
