<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePipProfileUserBanReason extends Migration
{
    public function up()
    {
        Schema::table('pip_profile_user_ban_reason', function($table)
        {
            $table->increments('id')->unsigned(false)->change();
            $table->renameColumn('ban_reason', 'reason');
        });
    }
    
    public function down()
    {
        Schema::table('pip_profile_user_ban_reason', function($table)
        {
            $table->increments('id')->unsigned()->change();
            $table->renameColumn('reason', 'ban_reason');
        });
    }
}
