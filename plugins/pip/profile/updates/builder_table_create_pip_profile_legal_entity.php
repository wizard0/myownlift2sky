<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePipProfileLegalEntity extends Migration
{
    public function up()
    {
        Schema::create('pip_profile_legal_entity', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('requisites');
            $table->integer('contracts');
            $table->string('payments');
            $table->integer('contact_persons');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pip_profile_legal_entity');
    }
}
