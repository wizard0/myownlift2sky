<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePipProfileCars extends Migration
{
    public function up()
    {
        Schema::table('pip_profile_cars', function($table)
        {
            $table->string('status', 10)->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('pip_profile_cars', function($table)
        {
            $table->integer('status')->nullable(false)->unsigned(false)->default(1)->change();
        });
    }
}
