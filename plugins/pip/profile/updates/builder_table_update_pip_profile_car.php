<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePipProfileCar extends Migration
{
    public function up()
    {
        Schema::rename('pip_profile_cars', 'pip_profile_car');
    }
    
    public function down()
    {
        Schema::rename('pip_profile_car', 'pip_profile_cars');
    }
}
