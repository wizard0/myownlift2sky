<?php namespace PIP\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePipProfileCarTypes extends Migration
{
    public function up()
    {
        Schema::rename('pip_profile_car_type', 'pip_profile_car_types');
        Schema::table('pip_profile_car_types', function($table)
        {
            $table->string('name')->change();
            $table->string('code')->change();
        });
    }
    
    public function down()
    {
        Schema::rename('pip_profile_car_types', 'pip_profile_car_type');
        Schema::table('pip_profile_car_type', function($table)
        {
            $table->string('name', 191)->change();
            $table->string('code', 191)->change();
        });
    }
}
