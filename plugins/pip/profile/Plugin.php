<?php
namespace PIP\Profile;

use Event;
use System\Classes\PluginBase;
use RainLab\User\Controllers\Users as UsersController;
use RainLab\User\Models\User as UsersModel;
use RainLab\User\Models\UserGroup as UserGroup;

class Plugin extends PluginBase
{

	public function registerComponents()
	{
		return [
		    'PIP\Profile\Components\RequestsOfCustomer' => 'requestsOfCustomer',
            'PIP\Profile\Components\RequestsOfOwner' => 'requestsOfOwner',
            'PIP\Profile\Components\RequestForm' => 'requestform',
            'PIP\Profile\Components\OwnerCars' => 'ownerCars',
        ];
	}

	public function registerSettings()
	{
		
	}

	public function boot()
	{
		// Add new fields -> START
		UsersController::extendFormFields(function($form, $model, $context) {
			$form->addTabFields([
				'phone' => [
					'label' => 'Телефон',
					'type' => 'text',
					'tab' => 'rainlab.user::lang.user.account'
				]
			]);
		});
		// Add new fields <- END
		// Set user group on register -> START
		UsersModel::extend(function($model) {
			// Add new function for group
			$model->addDynamicMethod('addUserGroup', function($group) use ($model) {
				if ($group instanceof Collection) {
					return $model->groups()->saveMany($group);
				}

				if (is_string($group)) {
					$group = UserGroup::whereCode($group)->first();

					return $model->groups()->save($group);
				}

				if ($group instanceof UserGroup) {
					return $model->groups()->save($group);
				}
			});

			// Add new fields phone and user_type
			$model->addFillable([
				'phone',
				//'user_type'
			]);
			$model->rules['phone'] = 'required|min:5';
		});

		Event::listen('rainlab.user.register', function($user, $data) {
			$aviable_groups = [
				'customer',
				'owner'
			];

			if (isset($data['acc_type']) and in_array($data['acc_type'], $aviable_groups)) {
				$user_group = UserGroup::whereCode($data['acc_type'])->first();
				if ($user_group) {
					$user->addUserGroup($user_group);
				}
			}
		});
		// Set user group on register <- END
	}
}
