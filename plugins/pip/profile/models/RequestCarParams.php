<?php namespace PIP\Profile\Models;

use Model;

/**
 * Model
 */
class RequestCarParams extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'param' => ['PIP\Profile\Models\CarTypeParams', 'key' => 'param_id']
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pip_profile_request_car_params';
}
