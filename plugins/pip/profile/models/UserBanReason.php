<?php namespace PIP\Profile\Models;

use Model;

/**
 * Model
 */
class UserBanReason extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'user' => 'RainLab\User\Models\User'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pip_profile_user_ban_reason';
}
