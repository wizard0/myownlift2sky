<?php namespace PIP\Profile\Models;

use Model;

/**
 * Model
 */
class CompletedRequests extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pip_profile_completed_requests';
}
