<?php namespace PIP\Profile\Models;

use Model;

/**
 * Model
 */
class Request extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    //public $timestamps = false;

    public $implement = ['RainLab.Location.Behaviors.LocationModel'];

    // TODO. Переделать отношения. Потому что это нифига не belongs
    // нужно наверное подобавлять в таблицы request_crane и user поля для ИД запросов
    public $belongsTo = [
        'crane' => 'PIP\Profile\Models\RequestCrane',
        'customer' => 'RainLab\User\Models\User', 'key' => 'customer_id',
        'owner' => 'RainLab\User\Models\User', 'key' => 'owner_id',
    ];

    public $hasMany = [
        'declined' => [
            'PIP\Profile\Models\DeclinedRequests'
        ],
        'completed' => [
            'PIP\Profile\Models\CompletedRequests'
        ],
        'canceled' => [
            'PIP\Profile\Models\CanceledRequests'
        ],
        'params' => [
            'PIP\Profile\Models\RequestCarParams'
        ]
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pip_profile_request';

    const REQUEST_STATUS_PENDING   = 'pending'  ;
    const REQUEST_STATUS_ACCEPTED  = 'accepted' ;
    const REQUEST_STATUS_DECLINED  = 'declined' ;
    const REQUEST_STATUS_COMPLETED = 'completed';

    public function getDateOnAttribute($value) {
        return date_format(date_create($value), "Y.m.d");
    }
}
