<?php namespace PIP\Profile\Models;

use Model;

/**
 * Model
 */
class CarType extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $hasMany = [
        'paramNames' => 'PIP\Profile\Models\CarTypeParams',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pip_profile_car_types';

    const CAR_TYPE_CRANE = 'crane';
}
