<?php namespace PIP\Profile\Models;

use Model;

/**
 * Model
 */
class Car extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'type' => ['PIP\Profile\Models\CarType', 'key' => 'car_type_id']
    ];

    public $hasMany = [
        'params' => ['PIP\Profile\Models\OwnerCarParams', 'delete' => true]
    ];
    public $hasOne = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pip_profile_car';

    const CAR_STATUS_FREE = 'free';
    const CAR_STATUS_BUSY = 'busy';
}
